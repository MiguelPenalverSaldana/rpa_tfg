import subprocess
import sys

subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", "requirements.txt"])

from PyQt6 import QtCore, QtGui, QtWidgets, QtQuick

class Ui_MainWindow(object):
    def __init__(self):
        self.path_listado = None
        self.path_restricciones = None
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(930, 590)
        MainWindow.setMinimumSize(QtCore.QSize(930, 590))
        MainWindow.setMaximumSize(QtCore.QSize(930, 590))
        MainWindow.setMouseTracking(False)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(30, 90, 498, 60))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.examinar_listado_tfg = QtWidgets.QPushButton(self.layoutWidget)
        self.examinar_listado_tfg.setMaximumSize(QtCore.QSize(76, 16777215))
        self.examinar_listado_tfg.setObjectName("examinar_listado_tfg")
        self.horizontalLayout_3.addWidget(self.examinar_listado_tfg)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.examinar_restricciones = QtWidgets.QPushButton(self.layoutWidget)
        self.examinar_restricciones.setMaximumSize(QtCore.QSize(76, 16777215))
        self.examinar_restricciones.setObjectName("examinar_restricciones")
        self.horizontalLayout_2.addWidget(self.examinar_restricciones)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.layoutWidget1 = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget1.setGeometry(QtCore.QRect(30, 20, 441, 56))
        self.layoutWidget1.setObjectName("layoutWidget1")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.layoutWidget1)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_5 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_5.addWidget(self.label_5)
        self.lineEdit = QtWidgets.QLineEdit(self.layoutWidget1)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout_5.addWidget(self.lineEdit)
        self.verticalLayout_4.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_6 = QtWidgets.QLabel(self.layoutWidget1)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.layoutWidget1)
        self.lineEdit_2.setEchoMode(QtWidgets.QLineEdit.EchoMode.Password)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_4.addWidget(self.lineEdit_2)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.ejecutar = QtWidgets.QPushButton(self.centralwidget)
        self.ejecutar.setGeometry(QtCore.QRect(410, 470, 75, 24))
        self.ejecutar.setMaximumSize(QtCore.QSize(300, 16777215))
        self.ejecutar.setObjectName("ejecutar")
        self.ejecutar.setEnabled(False)
        self.layoutWidget2 = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget2.setGeometry(QtCore.QRect(30, 180, 529, 180))
        self.layoutWidget2.setObjectName("layoutWidget2")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.layoutWidget2)
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_3 = QtWidgets.QLabel(self.layoutWidget2)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_6.addWidget(self.label_3)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_8 = QtWidgets.QLabel(self.layoutWidget2)
        self.label_8.setObjectName("label_8")
        self.verticalLayout.addWidget(self.label_8)
        self.bool_planificacion = QtWidgets.QCheckBox(self.layoutWidget2)
        self.bool_planificacion.setObjectName("bool_planificacion")
        self.verticalLayout.addWidget(self.bool_planificacion)
        self.bool_docencia = QtWidgets.QCheckBox(self.layoutWidget2)
        self.bool_docencia.setObjectName("bool_docencia")
        self.verticalLayout.addWidget(self.bool_docencia)
        self.bool_tutorias = QtWidgets.QCheckBox(self.layoutWidget2)
        self.bool_tutorias.setObjectName("bool_tutorias")
        self.verticalLayout.addWidget(self.bool_tutorias)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_4 = QtWidgets.QLabel(self.layoutWidget2)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout.addWidget(self.label_4)
        self.numero_aulas = QtWidgets.QSpinBox(self.layoutWidget2)
        self.numero_aulas.setMaximumSize(QtCore.QSize(50, 16777215))
        self.numero_aulas.setMinimum(1)
        self.numero_aulas.setObjectName("numero_aulas")
        self.horizontalLayout.addWidget(self.numero_aulas)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.verticalLayout_6.addLayout(self.verticalLayout_2)
        self.verticalLayout_7.addLayout(self.verticalLayout_6)
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.label_7 = QtWidgets.QLabel(self.layoutWidget2)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_11.addWidget(self.label_7)
        self.numero_cuatrimestre = QtWidgets.QSpinBox(self.layoutWidget2)
        self.numero_cuatrimestre.setMaximumSize(QtCore.QSize(50, 16777215))
        self.numero_cuatrimestre.setMinimum(1)
        self.numero_cuatrimestre.setMaximum(3)
        self.numero_cuatrimestre.setObjectName("numero_cuatrimestre")
        self.horizontalLayout_11.addWidget(self.numero_cuatrimestre)
        self.verticalLayout_7.addLayout(self.horizontalLayout_11)
        self.bool_mejora = QtWidgets.QCheckBox(self.centralwidget)
        self.bool_mejora.setGeometry(QtCore.QRect(30, 380, 800, 20))
        self.bool_mejora.setObjectName("bool_mejora")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.examinar_listado_tfg.clicked.connect(self.get_listado)
        self.examinar_restricciones.clicked.connect(self.get_restricciones)
        self.examinar_listado_tfg.clicked.connect(self.permitir_ejecucion)
        self.examinar_restricciones.clicked.connect(self.permitir_ejecucion)
        self.bool_tutorias.stateChanged.connect(self.permitir_ejecucion)
        self.bool_docencia.stateChanged.connect(self.permitir_ejecucion)
        self.bool_planificacion.stateChanged.connect(self.permitir_ejecucion)
        self.ejecutar.clicked.connect(self.iniciar)

        self.lineEdit.setStyleSheet('border: 1px solid grey')
        self.lineEdit_2.setStyleSheet('border: 1px solid grey')

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def permitir_ejecucion(self):
        if self.lineEdit.text() != '' and self.lineEdit_2.text() != '' and self.path_listado != None:
            if self.bool_tutorias.isChecked() or self.bool_planificacion.isChecked() or self.bool_docencia.isChecked():
                if self.bool_planificacion.isChecked() and self.path_restricciones == None:
                    self.ejecutar.setEnabled(False)
                else:
                    self.ejecutar.setEnabled(True)
            else:
                self.ejecutar.setEnabled(False)
        else:
            self.ejecutar.setEnabled(False)

    def get_restricciones(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(None, 'Open File', '', 'Archivos (*.xls *.xlsx *.csv)')

        if fname[0] != '':
            self.path_restricciones = fname[0]

    def get_listado(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(None, 'Open File', '', 'Archivos (*.xls *.xlsx *.csv)')

        if fname[0] != '':
            self.path_listado = fname[0]

    def iniciar(self):
        self.ejecutar.setEnabled(False)
        if self.path_listado != None:
            correo = self.lineEdit.text()
            contrasena = self.lineEdit_2.text()

            tutorias = 'True' if self.bool_tutorias.isChecked() else 'False'
            docencia ='True' if self.bool_docencia.isChecked() else 'False'
            planificacion = 'True' if self.bool_planificacion.isChecked() else 'False'
            mejora = 'True' if self.bool_mejora.isChecked() else 'False'
            aulas = str(self.numero_aulas.value())
            cuatrimestre = str(self.numero_cuatrimestre.value())

            if planificacion == 'True' and self.path_restricciones == None:
                mensaje_error(self, 1, 'Se ha marcado la opción de añadir las restricciones de los profesores pero no se ha aportado el fichero')
                return

            from horarios_tfg import horarios
            cod_error, msje_error = horarios(correo, contrasena, tutorias, docencia, planificacion, mejora, aulas, cuatrimestre, self.path_listado, self.path_restricciones)
            mensaje_error(self, cod_error, msje_error)

            self.path_restricciones = None
            self.path_listado = None
            self.ejecutar.setEnabled(True)
        else:
            self.ejecutar.setEnabled(True)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Horarios TFG"))
        self.label.setText(_translate("MainWindow", "Seleccione el archivo con el listado de los TFG que se van a presentar"))
        self.examinar_listado_tfg.setText(_translate("MainWindow", "Examinar"))
        self.label_2.setText(_translate("MainWindow", "Seleccione el archivo con las restricciones horarias rellenado por los profesores"))
        self.examinar_restricciones.setText(_translate("MainWindow", "Examinar"))
        self.label_5.setText(_translate("MainWindow", "Email UPM"))
        self.label_6.setText(_translate("MainWindow", "Contraseña"))
        self.ejecutar.setText(_translate("MainWindow", "Ejecutar"))
        self.label_3.setText(_translate("MainWindow", "Seleccione las opciones con las que desea que se ejecute el programa"))
        self.bool_planificacion.setText(_translate("MainWindow", "Tener en cuenta el archivo rellenado por los profesores con sus restricciones horarias"))
        self.bool_docencia.setText(_translate("MainWindow", "Tener en cuenta los horarios de docencia publicados en la web de tutorías"))
        self.bool_tutorias.setText(_translate("MainWindow", "Tener en cuenta los horarios de tutorías publicados en la web de tutorías"))
        self.label_4.setText(_translate("MainWindow", "Número de aulas disponibles para realizar las defensas de los TFG"))
        self.label_7.setText(_translate("MainWindow", "Cuatrimestre de las defensas de los TFG"))
        self.bool_mejora.setText(_translate("MainWindow", "Activar mejora de la calidad de las soluciones del algortimo (Tenga en cuenta que puede alargar considerablemente el tiempo de ejecución)"))
        self.label_8.setText(_translate("MainWindow", "Elija entre las siguientes opciones al menos una para determinar la disponibilidad de los profesores:"))

def mensaje_error(self, cod_error, msje_error):
    codigos_error = {
    0 : 'Info',
    1 : 'Aviso',
    2 : 'Error'
    }
    icono_error = {
        'Error' : QtWidgets.QMessageBox.Icon.Critical,
        'Aviso' : QtWidgets.QMessageBox.Icon.Warning,
        'Info' : QtWidgets.QMessageBox.Icon.Information
    }
    mensaje = QtWidgets.QMessageBox()
    mensaje.setText(msje_error)
    mensaje.setWindowTitle(codigos_error[cod_error])
    mensaje.setIcon(icono_error[codigos_error[cod_error]])
    mensaje.exec()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    app.setWindowIcon(QtGui.QIcon('IconoCalendario.png'))
    
    app.setStyle('Fusion')

    paleta_colores = QtGui.QPalette()
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Window, QtGui.QColor(53,53,53))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.WindowText, QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled, QtGui.QPalette.ColorRole.WindowText, QtGui.QColor(127,127,127))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Base, QtGui.QColor(42,42,42))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.AlternateBase, QtGui.QColor(66,66,66))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.ToolTipBase, QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.ToolTipText, QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Text, QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled, QtGui.QPalette.ColorRole.Text,QtGui.QColor(127,127,127))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Dark,QtGui.QColor(35,35,35))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Shadow,QtGui.QColor(20,20,20))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Button,QtGui.QColor(53,53,53))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.ButtonText,QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled, QtGui.QPalette.ColorRole.Button,QtGui.QColor(60,60,60))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled, QtGui.QPalette.ColorRole.ButtonText,QtGui.QColor(140,140,140))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled, QtGui.QPalette.ColorRole.Text,QtGui.QColor(127,127,127))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.BrightText,QtGui.QColor(QtGui.QColorConstants.Red))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Link,QtGui.QColor(42,130,218))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.Highlight,QtGui.QColor(42,130,218))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled,QtGui.QPalette.ColorRole.Highlight,QtGui.QColor(80,80,80))
    paleta_colores.setColor(QtGui.QPalette.ColorRole.HighlightedText,QtGui.QColor(QtGui.QColorConstants.White))
    paleta_colores.setColor(QtGui.QPalette.ColorGroup.Disabled,QtGui.QPalette.ColorRole.HighlightedText,QtGui.QColor(127,127,127))
    app.setPalette(paleta_colores)

    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec())