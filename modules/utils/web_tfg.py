from RPA.FileSystem import FileSystem
from RPA.Excel.Files import Files
from RPA.Browser.Selenium import Selenium
from copy import deepcopy
import time

#Inicializar librerías
browser = Selenium()
excel = Files()

def formatear_nombres(nombres):
    for key in nombres:
        cambios = {'á' : 'a', 'é' : 'e', 'í' : 'i', 'ó' : 'o', 'ú' : 'u', 'Á' : 'A', 'É' : 'E', 'Í' : 'I', 'Ó' : 'O', 'Ú' : 'U'}
        nombres[key] = nombres[key].title()
        for original, cambio in cambios.items():
                nombres[key] = nombres[key].replace(original, cambio)
        nombres[key] = nombres[key].split(',')
        nombres[key].reverse()
        nombres[key][1] = nombres[key][1].replace(' ','_')
        nombres[key][0] = nombres[key][0].replace(' ','',1)
        nombres[key][0] = nombres[key][0].replace(' ','_')
        nombres[key] = '_'.join(nombres[key])
        nombres[key] = nombres[key].split('_')

        for j in nombres[key]:
            if len(j) <= 2:
                del nombres[key][nombres[key].index(j)]

        nombres[key] = '_'.join(nombres[key])

#Abre la url en una nueva ventana del navegador
def abrir_web(url, h):
    cod_error = 0
    msje_error = ''
    try:
        browser.open_available_browser(url, headless=h)
    except:
        cod_error = 2
        msje_error = 'No ha sido posible abrir el navegador web, compruebe que hay algún navegador abierto y que la página web {} está disponible'.format(url)

    return cod_error, msje_error

def login(login_text,password_text):
    cod_error = 0
    msje_error = ''
    try:
        browser.input_text("name:username",login_text)
        browser.input_text("name:password",password_text)
        browser.click_element("id:button-1016")
        browser.wait_until_element_is_visible("id:tbtext-1041", timeout=10)
    except:
        cod_error = 2
        msje_error = 'No ha sido posible acceder a la web de los TFG. Compruebe que el correo y la constraseña son correctos.'

    return cod_error, msje_error

def get_numero():
    cod_error = 0
    msje_error = ''
    try:
        browser.wait_until_element_is_visible("id:tbtext-1041", timeout=1)
        text = browser.get_text("id:tbtext-1041")
        aux = text.split(" ")
        numero_pag = int(aux[-1])

        browser.wait_until_element_is_visible("id:tbtext-1048", timeout=10)
        text = browser.get_text("id:tbtext-1048")
        aux = text.split(" ")
        numero_tfg = int(aux[-1])
    except:
        numero_tfg = 0
        numero_pag = 0
        cod_error = 2
        msje_error = 'Error leyendo los miembros de cada TFG. No ha sido posible obtener el nñumero de tfgs totales.'
    
    return [numero_pag, numero_tfg], cod_error, msje_error


def crear_excel(lista):
    excel.create_workbook('Hoja1', fmt = "xlsx")
    excel.append_rows_to_worksheet(lista,"Sheet")
    excel.save_workbook("tfgs.xlsx")

def listado(path_listado):
    cod_error = 0
    msje_error = ''
    try:
        excel.open_workbook(path_listado)
        l = excel.read_worksheet( header = True)
        return l, cod_error, msje_error
    except:
        cod_error = 2
        msje_error = 'No se ha podido abrir el archivo {}'.format(path_listado)
        return 0, cod_error, msje_error

def desconectar():
        browser.click_element("id:button-1020")

def pasar_pagina():
        browser.wait_until_element_is_visible("id:button-1043")
        browser.click_element("id:button-1043")

def leer_TFG(i,n):
    aux=False
    while not aux:
        try:
            browser.click_element('xpath://*[@id="gridview-1032-body"]/tr['+str(i+1)+']/td[1]')
            aux = True
        except:
            pass
    
    browser.click_element("id:button-1034")


    if n==1:
        aux = 8
    else:
        aux = 9

    try:
        browser.wait_until_element_is_visible('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]')
    except:
        while True:
            pass
    try:
        #Primer miembro tribunal
        browser.press_keys('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[14]/tbody/tr/td[2]/table/tbody/tr/td[1]/input', "CTRL"+"a")
        miembro1 = browser.get_value('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[14]/tbody/tr/td[2]/table/tbody/tr/td[1]/input')

        #Segundo miembro tribunal
        browser.press_keys('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[15]/tbody/tr/td[2]/table/tbody/tr/td[1]/input', "CTRL"+"a")
        miembro2 = browser.get_value('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[15]/tbody/tr/td[2]/table/tbody/tr/td[1]/input')

        #Tecer miembro tribunal
        browser.press_keys('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[16]/tbody/tr/td[2]/table/tbody/tr/td[1]/input', "CTRL"+"a")
        miembro3 = browser.get_value('xpath:/html/body/div['+str(aux)+']/div[2]/div/div[1]/span/div/table[16]/tbody/tr/td[2]/table/tbody/tr/td[1]/input')

        tribunal = {'Miembro 1' : miembro1, 'Miembro 2' : miembro2, 'Miembro 3' : miembro3}
    finally:
        browser.click_element('xpath: /html/body/div['+str(aux)+']/div[2]/div/div[2]/div/div/a')
        browser.wait_until_element_is_visible('id:button-1034')

        return tribunal

def buscar_TFG(titulos_formato,titulos_sin_formato, n_pags, n_tfg, lista_tfg, formato_titulo):
    cod_error = 0
    msje_error = ''

    k = 1
    n_leidos = 0
    n_leer = len(titulos_formato)

    try:
        for pag in range(1,n_pags+1):
            tfgs_pagina = browser.find_elements('xpath://*[@id="gridview-1032-body"]/tr')
            for i_tfg in range(len(tfgs_pagina)):
                aux = False
                while not aux:
                    try:
                        titulo = browser.get_text('xpath://*[@id="gridview-1032-body"]/tr['+str(i_tfg+1)+']/td[2]')
                        aux = True
                    except:
                        aux = False

                titulo = titulo.lower()
                for key, value in formato_titulo.items():
                    titulo = titulo.replace(key, value)
                if titulo[-1] == 's':
                    titulo = list(titulo)
                    del titulo[-1]
                    titulo = ''.join(titulo)
                titulo = titulo.replace(' ','')

                for titulo_formato in titulos_formato:
                    if titulo_formato == titulo:
                        #Obtener la fecha
                        browser.wait_until_element_is_enabled('xpath://*[@id="gridview-1032-body"]/tr['+str(i_tfg+1)+']/td[7]')
                        browser.wait_until_element_is_visible('xpath://*[@id="gridview-1032-body"]/tr['+str(i_tfg+1)+']/td[7]')
                        try:
                            fecha_txt = browser.get_text('xpath://*[@id="gridview-1032-body"]/tr['+str(i_tfg+1)+']/td[7]')
                        except:
                            fecha_txt = browser.get_text('xpath://*[@id="gridview-1032-body"]/tr['+str(i_tfg+1)+']/td[7]')

                        info = leer_TFG(i_tfg,n_leidos+1)
                        formatear_nombres(info)
                        aux = {'Título' : titulos_sin_formato[titulos_formato.index(titulo_formato)]}
                        aux.update(info)
                        aux['Fecha'] = fecha_txt
                        lista_tfg.append(deepcopy(aux))
                        del aux['Fecha']

                        n_leidos = n_leidos+1
                        break

                k = k+1
                if k == n_tfg:
                    return cod_error, msje_error

            if pag != n_pags :
                pasar_pagina()
    except:
        cod_error = 2
        msje_error = 'Error en la búsqueda de los miembross del tribunal de los TFG en la web'
    
    return cod_error, msje_error

def tfg(url_tfgs, path_listado, correo, contrasena, headless):
    cod_error = 0
    msje_error = ''

    lista=[["Título","Primer miembro Tribunal","Segundo miembro Tribunal","Tercer miembro Tribunal", "Fecha", "Alumno"]]
    try:
        if FileSystem().does_file_not_exist("tfgs.xlsx") == True:
            crear_excel(lista)
        else:
            FileSystem().remove_file("tfgs.xlsx")
            crear_excel(lista)
    except PermissionError:
        cod_error = 1
        msje_error = 'El archivo tfgs.xlsx está abierto, ciérrelo e intente ejecutar el programa de nuevo'
        return {}, [], cod_error, msje_error

    #Abre la web
    cod_error, msje_error = abrir_web(url_tfgs, headless)
    if cod_error!= 0:
        return {}, [], cod_error, msje_error

    #Realiza el login
    cod_error, msje_error = login(correo,contrasena)
    if cod_error!= 0:
        return {}, [], cod_error, msje_error

    #Obtiene el número total de páginas y TFG
    [n_pags, n_tfg], cod_error, msje_error = get_numero()
    if cod_error!= 0:
        return {}, [], cod_error, msje_error 
    
    #Abre y lee el listado de TFG
    l, cod_error, msje_error = listado(path_listado)
    if cod_error!= 0:
        return {}, [], cod_error, msje_error 

    keys = []
    for key in l[1].keys():
        keys.append(key) 

    titulos_formato=[]
    titulos_sin_formato=[]
    alumnos = {}

    for dic in l:
        if dic[keys[1]] != '':
            alumnos[dic[keys[1]]] = dic[keys[0]]
            aux = dic[keys[1]]
            titulos_sin_formato.append(aux)
            aux = aux.lower()
            formato_titulo = {'á' : 'a', 'é' : 'e', 'í' : 'i', 'ó' : 'o', 'ú' : 'u', '.' : '', ' de ' : '   ', ' del ' : '  ', ' el ':'  ', ' las ':'  ',
                 ' los ':'  ', ' la ':'  ', ' para ':'  ', ' un ':'  ', ' una ':'  ', ' a ':'  ', ' en ':'  ', ' y ':'  ', ',':'', 's ':' '}
            for key, value in formato_titulo.items():
                aux = aux.replace(key, value)
            if aux[-1] == 's':
                aux = list(aux)
                del aux[-1]
                aux = ''.join(aux)
            aux = aux.replace(' ','')
            titulos_formato.append(aux)

    lista_tfg = []
    cod_error, msje_error = buscar_TFG(titulos_formato,titulos_sin_formato, n_pags, n_tfg, lista_tfg, formato_titulo)
    if cod_error!= 0:
        return {}, [], cod_error, msje_error 

    #Eliminar los repetidos, quedándose con el más actual
    for tfg in lista_tfg:
        tfg['Alumno'] = alumnos[tfg['Título']]
        for tfg_2 in lista_tfg:
            if tfg['Título'] == tfg_2['Título']:
                if time.strptime(tfg['Fecha'], "%d/%m/%Y") < time.strptime(tfg_2['Fecha'], "%d/%m/%Y"):
                    del lista_tfg[lista_tfg.index(tfg)]
                elif time.strptime(tfg['Fecha'], "%d/%m/%Y") > time.strptime(tfg_2['Fecha'], "%d/%m/%Y"):
                    del lista_tfg[lista_tfg.index(tfg_2)]
    try:
        excel.open_workbook("tfgs.xlsx")
        excel.append_rows_to_worksheet(lista_tfg,"Sheet")
        excel.save_workbook("tfgs.xlsx")
    except:
        cod_error = 2
        msje_error = 'Error al abrir el archivo tfgs.xlsx'
        return {}, [], cod_error, msje_error

    #Indicar qué TFG no aparecen en la web
    lista_tfg_no_web = []
    for tfg in lista_tfg:
        if tfg['Título'] in titulos_sin_formato:
            del titulos_sin_formato[titulos_sin_formato.index(tfg['Título'])]

    for titulo in titulos_sin_formato:
        lista_tfg_no_web.append(titulo)

    #Convertir lista en un diccionario
    dicts_tfg = {}

    for tfg in lista_tfg:
        dicts_tfg[tfg['Título']] = tfg
        tfg.pop('Título', None)

    desconectar()
    browser.close_browser()
    return dicts_tfg, lista_tfg_no_web, cod_error, msje_error