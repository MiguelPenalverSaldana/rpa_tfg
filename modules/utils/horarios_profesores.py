from RPA.Browser.Selenium import Selenium
from RPA.HTTP import HTTP
from RPA.FileSystem import FileSystem

#Inicializa ambas librerias
browser = Selenium()
http = HTTP()

def tutorias(nombres, url_tutorias, directorio_resources, headless):
    cod_error = 0
    msje_error = ''

    lista_profesores_sin_horario = []

    cod_error, msje_error = abrir_web(url_tutorias, headless)
    if cod_error != 0:
        return [], cod_error, msje_error

    try:
        #Se recorre la lista de nombres de profesores y se llama a la función que descarga su horario
        for i in range(len(nombres)):
            datos_profesor(nombres[i].replace('_',' '), nombres[i], directorio_resources, i, lista_profesores_sin_horario)
        #Cierra el navegador que se ha abierto
        browser.close_browser()
    except:
        cod_error = 2
        msje_error = 'Error obteniendo el horario del profesor ' + nombres[i] + '.'

    return [], cod_error, msje_error

#Abre la url en una nueva ventana del navegador
def abrir_web(url, h):
    cod_error = 0
    msje_error = ''
    try:
        browser.open_available_browser(url, headless=h)
    except:
        cod_error = 2
        msje_error = 'No ha sido posible abrir la web {}, compruebe que hay algún navegador abierto'.format(url)

    return cod_error, msje_error

#Descarga el excel correspondiente a los horarios del profesor con el nombre indicado
def datos_profesor(nombre, nombre_formato, directorio_resources, i, lista_profesores_sin_horario):
    while browser.get_value('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]/input') != '':
        browser.click_element('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]')
        browser.press_keys( None,"BACKSPACE")

    browser.press_keys( browser.find_element('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div'),nombre)
    browser.press_keys( None,"ENTER")

    if i==0:
        try:
            browser.wait_until_element_is_visible('id:dExcel', timeout = 1)

            href = browser.get_element_attribute(browser.find_element('id:dExcel'), 'href')
            http.download(url = href, target_file = directorio_resources+nombre_formato+".xlsx", overwrite = True, force_new_session = True, stream = True)
        except:
            try:
                browser.click_element('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]')
                browser.press_keys( None,"BACKSPACE")

                browser.press_keys( browser.find_element('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div'),nombre)
                browser.press_keys( None,"ENTER")

                browser.wait_until_element_is_visible('id:dExcel', timeout = 1)

                href = browser.get_element_attribute(browser.find_element('id:dExcel'), 'href')
                http.download(url = href, target_file = directorio_resources+nombre_formato+".xlsx", overwrite = True, force_new_session = True, stream = True)
            except:
                pass
    else:
        try:
            browser.wait_until_element_is_visible('id:dExcel', timeout = 1)

            href = browser.get_element_attribute(browser.find_element('id:dExcel'), 'href')
            http.download(url = href, target_file = directorio_resources+nombre_formato+".xlsx", overwrite = True, force_new_session = True, stream = True) 
        except:
            lista_profesores_sin_horario.append(nombre)

    browser.click_element('xpath:/html/body/div[1]/div[2]/div[1]/div[1]/div/div/div/div[1]')
    browser.press_keys( None,"BACKSPACE")
    browser.wait_until_element_is_not_visible('id:dExcel', timeout = 1)