import pandas as pd
from datetime import datetime
from copy import deepcopy

def restricciones(path_restricciones, nombre):
    cod_error = 0
    msje_error = ''
    try:
        #Lectura del archivo
        disponibilidad = pd.read_csv(path_restricciones, sep=',',header=None, engine='python')
        disponibilidad = disponibilidad.drop([0],axis=1)
        disponibilidad.columns = range(disponibilidad.shape[1])

        #Simplificación
        for i in disponibilidad.columns:
            if disponibilidad[i][0].find("[") != -1 and disponibilidad[i][0].find("]") != -1:
                inicio = disponibilidad[i][0].find("[") + len("[")
                fin = disponibilidad[i][0].find("]")
                substring = disponibilidad[i][0][inicio:fin]
                substring_2 = substring.split(' ')[0:2]
                disponibilidad[i][0] = ' '.join(substring_2)

        #Formatear nombres para poder buscar posteriormente
        for i in range(len(disponibilidad[1])):
            if disponibilidad[1][i][-1] == ' ':
                disponibilidad[1][i] = list(disponibilidad[1][i])
                del disponibilidad[1][i][-1]
                disponibilidad[1][i] = ''.join(disponibilidad[1][i])
            disponibilidad[1][i] = disponibilidad[1][i].title()
            disponibilidad[1][i] = disponibilidad[1][i].replace(' ','_')
            disponibilidad[1][i] = disponibilidad[1][i].replace('á','a')
            disponibilidad[1][i] = disponibilidad[1][i].replace('é','e')
            disponibilidad[1][i] = disponibilidad[1][i].replace('í','i')
            disponibilidad[1][i] = disponibilidad[1][i].replace('ó','o')
            disponibilidad[1][i] = disponibilidad[1][i].replace('ú','u')
            disponibilidad[1][i] = disponibilidad[1][i].replace('Á','A')
            disponibilidad[1][i] = disponibilidad[1][i].replace('É','E')
            disponibilidad[1][i] = disponibilidad[1][i].replace('Í','O')
            disponibilidad[1][i] = disponibilidad[1][i].replace('Ó','I')
            disponibilidad[1][i] = disponibilidad[1][i].replace('Ú','U')

        #En caso de repetición de un profesor, se elimina la entrada más antigua y se queda con la más recient
        for j in range(1,len(disponibilidad[1])):
            for k in range(j+1,len(disponibilidad[1])):
                if disponibilidad[1][j]==disponibilidad[1][k] or disponibilidad[1][j]==disponibilidad[1][k]:
                    disponibilidad=disponibilidad.drop(j)
                    break
                    pass

        disponibilidad = disponibilidad.reset_index(drop=True)
        disponibilidad = disponibilidad.drop([0],axis=1)
        disponibilidad.columns = range(disponibilidad.shape[1])
        disponibilidad = disponibilidad.iloc[: , :-1]

        aux = False
        for i in range(1,len(disponibilidad[0])):
            interseccion = nombre.intersection(disponibilidad[0][i].split('_'))
            if len(list(interseccion)) >= 2:
                df_restricciones = disponibilidad.iloc[[0,i]]
                df_restricciones = df_restricciones.drop([0],axis=1)
                df_restricciones.columns = range(df_restricciones.shape[1])
                header = df_restricciones.iloc[0]
                df_restricciones = df_restricciones[1:]
                df_restricciones.columns = header
                df_restricciones = df_restricciones.reset_index(drop=True)
                aux = True
        if not aux:
            df_restricciones = pd.DataFrame()
    except:
        df_restricciones = pd.DataFrame()
        cod_error = 2
        msje_error = "No ha sido posible leer el fichero {}".format(path_restricciones)
    finally:
        return df_restricciones, cod_error, msje_error