from datetime import datetime, timedelta
from RPA.Browser.Selenium import Selenium
from RPA.HTTP import HTTP
from RPA.FileSystem import FileSystem
import fitz

meses ={
    'enero' : '1',
    'febrero' : '2',
    'marzo' : '3',
    'abril' : '4',
    'mayo' : '5',
    'junio' : '6',
    'julio' :  '7',
    'agosto' : '8',
    'septiembre' : '9',
    'octubre' : '10',
    'noviembre' : '11',
    'diciembre' : '12',
    'e' : 'error'
}

dias = {
    0 : 'Lunes',
    1 : 'Martes',
    2 : 'Miércoles',
    3 : 'Jueves',
    4 : 'Viernes'
}

#Inicializa ambas librerias
browser = Selenium()
http = HTTP()

#Abre la url en una nueva ventana del navegador
def abrir_web(url, h):
    cod_error = 0
    msje_error = ''
    try:
        browser.open_available_browser(url, headless=h)
    except:
        cod_error = 2
        msje_error = 'No ha sido posible abrir el navegador web, compruebe que hay algún navegador abierto'

    return cod_error, msje_error

def fechas(url_calendario, directorio_resources, headless, cuatrimestre, fecha_inicio):
    cod_error = 0
    msje_error = ''

    cod_error, msje_error = abrir_web(url_calendario, headless)

    try:
        #Descargar calendario ETSIDI
        if cuatrimestre == 1:
            href = browser.get_element_attribute('xpath:/html/body/div[4]/div/div[2]/div/div[1]/div/div/ul[1]/li[1]/a[1]', 'href')
        elif cuatrimestre == 2:
            href = browser.get_element_attribute('xpath:/html/body/div[4]/div/div[2]/div/div[1]/div/div/ul[1]/li[2]/p/a[1]', 'href')
        else:
            href = browser.get_element_attribute('xpath:/html/body/div[4]/div/div[2]/div/div[1]/div/div/ul[1]/li[3]/p/a[1]', 'href')

        http.download(url = href, target_file = directorio_resources+"calendario.pdf", overwrite = True, force_new_session = True, stream = True)
        
        #Leer el texto del PDF y almacenarlo
        with fitz.open(directorio_resources+"calendario.pdf") as doc:
            text = ''
            for page in doc:
                text += page.get_text()#.encode("utf8")

        #Encontrar coincidencias de una cadena de caracteres concreta para encontrar cuando son las defensas de los TFG
        encontrar_inicio = 'Fecha de defensa del TFG: del '
        encontrar_anno = 'CURSO '

        res1 = [i for i in range(len(text)) if text.startswith(encontrar_inicio, i)]
        res3 = [i for i in range(len(text)) if text.startswith(encontrar_anno, i)]

        if not res1:
            encontrar_inicio = 'Fecha de defensa del TFG:  del '

        res1 = [i for i in range(len(text)) if text.startswith(encontrar_inicio, i)]

        curso = text[res3[0]+len(encontrar_anno):res3[0]+len(encontrar_anno)+9].split('-')[1]

        if cuatrimestre == 1 or cuatrimestre == 3:
            dia_inicio = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+25].split(' de ')[0].split(' al ')[0]
            dia_fin = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+25].split(' de ')[0].split(' al ')[1]
            mes = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+25].split(' de ')[1].split('\n')[0].replace(' ','')
            inicio_aux = datetime.strptime(dia_inicio+'-'+meses[mes]+'-'+curso, '%d-%m-%Y')
            fin_aux = datetime.strptime(dia_fin+'-'+meses[mes]+'-'+curso, '%d-%m-%Y')
        else:
            dia_inicio = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+30].split(' al ')[0].split(' de ')[0]
            dia_fin = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+30].split(' al ')[1].split('\n')[0].split(' de ')[0]
            mes1 = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+30].split(' al ')[0].split(' de ')[1]
            mes2 = text[res1[0]+len(encontrar_inicio):res1[0]+len(encontrar_inicio)+30].split(' al ')[1].split('\n')[0].split(' de ')[1].replace(' ','')
            inicio_aux = datetime.strptime(dia_inicio+'-'+meses[mes1]+'-'+curso, '%d-%m-%Y')
            fin_aux = datetime.strptime(dia_fin+'-'+meses[mes2]+'-'+curso, '%d-%m-%Y')

        aux = datetime.strptime(fecha_inicio, '%d-%m-%Y')
        dias_formato = {}
        while aux <= fin_aux:
            aux2 = dias[aux.weekday()] + ' ' + datetime.strftime(aux,'%d')
            dias_formato[aux] = aux2.replace(' 0', ' ')
            if aux.weekday() == 4:
                aux += timedelta(days=3)
            else:
                 aux += timedelta(days=1)
    except:
        cod_error = 2
        msje_error = "No ha sido posible descargar el calendario de fechas de defensa, intente ejecutar el programa de nuevo"
        dia_inicio = ''
        mes='e'
        dia_fin = ''
        curso = ''
        dias_formato = {}
    finally:
        #Cierra el navegador que se ha abierto
        browser.close_browser()

    return inicio_aux, fin_aux, dias_formato, cod_error, msje_error