import pandas as pd
import xml.etree.ElementTree as ET
import multiprocessing as mp
from random import choice, shuffle
from copy import deepcopy
from modules.utils.web_tfg import tfg, listado
from modules.utils.restricciones_profesores import restricciones
from modules.utils.horarios_profesores import tutorias
from modules.utils.fechas_defensa import fechas
from datetime import datetime, timedelta
from RPA.FileSystem import FileSystem
from numpy import nan

import pprint

def leer_configuracion(config_dict):
    #Leer desde archivo configuracion xml
    config = ET.parse('configuracion.xml')

    root = config.getroot()

    for i in root:
        for j in i:
            config_dict[j.attrib['name']] = j.text

def dataframe_horarios_profesor(nombre, bool_tutorias, bool_docencia, bool_planificacion, path_restricciones, directorio_resources):
    cod_error = 0
    msje_error = ''
    if bool_docencia == True or bool_tutorias == True:
        if FileSystem().does_file_exist(directorio_resources+nombre+'.xlsx'):
            #Leer horarios docencia
            if bool_docencia:
                df1 = pd.read_excel(directorio_resources+nombre+'.xlsx', sheet_name = 'Docencia', usecols = ['Dia','HoraInicio','HoraFinal'])
                df1.rename(columns={'HoraInicio': 'Desde', 'HoraFinal': 'Hasta'}, inplace = True)
            else:
                df1 = pd.DataFrame()

            #Leer horarios tutorías
            if bool_tutorias:
                df2 = pd.read_excel(directorio_resources+nombre+'.xlsx', sheet_name = 'Tutoria', usecols = ['Dia','Desde','Hasta'])
                df3 = pd.read_excel(directorio_resources+nombre+'.xlsx', sheet_name = 'TFG', usecols = ['Dia','Desde','Hasta'])
            else:
                df2 = pd.DataFrame()
                df3 = pd.DataFrame()
        else:
            df1 = pd.DataFrame()
            df2 = pd.DataFrame()
            df3 = pd.DataFrame()

    else:
        df1 = pd.DataFrame()
        df2 = pd.DataFrame()
        df3 = pd.DataFrame()    
    

    #Leer horarios planificación
    if bool_planificacion:
        df_restricciones, cod_error, msje_error = restricciones(path_restricciones, set(nombre.split('_')))
    else:
        df_restricciones = pd.DataFrame()

    frames = [df1,df2,df3]
    df_docencia_tutorias = pd.concat(frames)
    df_docencia_tutorias = df_docencia_tutorias.reset_index(drop=True)

    return {'df_docencia_tutorias' : df_docencia_tutorias, 'df_restricciones' : df_restricciones}, cod_error, msje_error

def calendario(hora_inicio, hora_fin, hora_inicio_descanso, hora_fin_descanso, duracion, n_aulas, lista_dias, turnos_defensa):
    #Crear lista de horas de defensa disponibles en un día
    horas_defensa = []
    inicio = datetime.strptime(hora_inicio, '%H:%M')
    fin = datetime.strptime(hora_fin, '%H:%M')
    inicio_descanso = datetime.strptime(hora_inicio_descanso, '%H:%M')
    fin_descanso = datetime.strptime(hora_fin_descanso, '%H:%M')
    duracion = datetime.strptime(duracion, '%H:%M')
    time_zero = datetime.strptime('00:00', '%H:%M')

    aux = inicio
    while aux < fin:
        if(aux < inicio_descanso or aux >= fin_descanso):
            horas_defensa.append([aux, (aux - time_zero + duracion)])
        aux = aux - time_zero + duracion

    turnos_por_dia = {}
    for dia in lista_dias:
        turnos_por_dia[dia] = deepcopy(horas_defensa)

    for i in range(int(n_aulas)):
        turnos_defensa['Aula '+str(i+1)] = deepcopy(turnos_por_dia)

def eliminar_horarios(tfg_dict, turnos_defensa, bool_tutorias, bool_docencia, bool_planificacion):
    turnos = deepcopy(turnos_defensa)

    if bool_planificacion:
        dfs_miembros_restricciones = []

        dfs_miembros_restricciones.append(tfg_dict['Miembro 1']['Dataframes']['df_restricciones'])
        dfs_miembros_restricciones.append(tfg_dict['Miembro 2']['Dataframes']['df_restricciones'])
        dfs_miembros_restricciones.append(tfg_dict['Miembro 3']['Dataframes']['df_restricciones'])

        for df in dfs_miembros_restricciones:
            for dia in df:
                if not pd.isnull(df[dia][0]):
                    lista = df[dia][0].split(';')

                    for horas in lista:
                        horas = horas.replace(' ','')
                        inicio = datetime.strptime(horas.split('-')[0], '%H:%M')
                        fin = datetime.strptime(horas.split('-')[1], '%H:%M')
                        for aula in turnos:
                            if dia in turnos[aula]:
                                for turno in turnos[aula][dia]:
                                    aux = -1
                                    aux2 = -1
                                    if inicio >= turno[0] and inicio < turno[1]:
                                        aux = turno
                                    if fin > turno[0] and fin <= turno[1]:
                                        aux2 = turno
                                    if aux != -1 and aux2 != -1:
                                        if aux != aux2:
                                            del turnos[aula][dia][turnos[aula][dia].index(aux)-1:turnos[aula][dia].index(aux2)]
                                        else:
                                            del turnos[aula][dia][turnos[aula][dia].index(aux)]
    
    if bool_docencia or bool_tutorias:
        dfs_miembros_tutorias = []

        dfs_miembros_tutorias.append(tfg_dict['Miembro 1']['Dataframes']['df_docencia_tutorias'])
        dfs_miembros_tutorias.append(tfg_dict['Miembro 2']['Dataframes']['df_docencia_tutorias'])
        dfs_miembros_tutorias.append(tfg_dict['Miembro 3']['Dataframes']['df_docencia_tutorias'])

        columnas = list(turnos['Aula 1'].keys())
        aulas = list(turnos.keys())

        for df in dfs_miembros_tutorias:
            elim_dict = {}
            for index, row in df.iterrows():
                inicio = datetime.strptime(row['Desde'], '%H:%M')
                fin = datetime.strptime(row['Hasta'], '%H:%M')

                if row['Dia'] in elim_dict:
                    elim_dict[row['Dia']].append([inicio, fin])
                else:
                    elim_dict[row['Dia']] = [[inicio, fin]]

                for aula in turnos:
                    for dia in turnos[aula]:
                        if dia.split(' ')[0] == row['Dia']:
                            aux = -1
                            aux2 = -1
                            for turno in turnos[aula][dia]:
                                if inicio >= turno[0] and inicio < turno[1]:
                                    aux = turno
                                if fin > turno[0] and fin <= turno[1]:
                                    aux2 = turno
                                if aux != -1 and aux2 != -1:
                                    if aux != aux2:
                                        del turnos[aula][dia][turnos[aula][dia].index(aux)-1:turnos[aula][dia].index(aux2)]
                                        aux = -1
                                        aux2 = -1
                                    else:
                                        del turnos[aula][dia][turnos[aula][dia].index(aux)]
                                        aux = -1
                                        aux2 = -1
    return turnos

def eliminar_tfg_sin_disponibilidad(tfg_dicts):
    for tfg_dict in list(tfg_dicts):
        horarios_disponibles = False

        for aula in tfg_dicts[tfg_dict]['Turnos']:
            for dia in tfg_dicts[tfg_dict]['Turnos'][aula]:
                if tfg_dicts[tfg_dict]['Turnos'][aula][dia]:
                    horarios_disponibles = True

        if not horarios_disponibles:
            print(tfg_dict + ' no tiene ningún horario disponible')
            del tfg_dicts[tfg_dict]

def ordenar_tfgs(tfg_dicts):
    n_turnos = {}

    lista_tfg_random = list(tfg_dicts.keys())
    shuffle(lista_tfg_random)

    for tfg_dict in lista_tfg_random:
        if 'Turno Asignado' not in tfg_dicts[tfg_dict].keys():
            valor = 0
            for dia in tfg_dicts[tfg_dict]['Turnos']['Aula 1']:
                if tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia]:
                    for hora in tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia]:
                        valor = valor + 1
            n_turnos[tfg_dict] = valor
    
    return sorted(n_turnos.items(), key=lambda x: x[1], reverse=False)

def asignar_horarios(tfg_dicts, turnos_defensa, profesores, dia_inicio_defensas, dia_fin_defensas, bool_mejores_soluciones):
    try:
        lista_tfg_ordenados = ordenar_tfgs(tfg_dicts)

        #Leer excel con nombres de los TFGs que se van a asignar
        df = pd.read_excel('tfgs.xlsx')
        df = df.drop(df.columns[4], axis = 1)
        df.insert(4, "Turno Asignado", 'Horario no disponible')
        df = df.reset_index()

        dias_sin_formato = {}
        for dia in turnos_defensa['Aula 1'].keys():
            dias_sin_formato[dia.split(' ')[1]] = dia.split(' ')[0]

        aux = dia_inicio_defensas
        dias_formato = {}
        for dia in dias_sin_formato:
            dias_formato[dias_sin_formato[dia]+' '+dia] = aux

            if dias_sin_formato[dia] == 'Viernes':
                aux = aux + timedelta(days=3)
            else:
                aux = aux + timedelta(days=1)
        del dias_sin_formato
        
        dia_preferente, hora_preferente = funcion_valor_primer_tfg(lista_tfg_ordenados[0][0], tfg_dicts)
        tfg_dicts[lista_tfg_ordenados[0][0]]['Turno Asignado'] = ['Aula 1', dia_preferente, tfg_dicts[lista_tfg_ordenados[0][0]]['Turnos']['Aula 1'][dia_preferente][0][0].strftime('%H:%M'), tfg_dicts[lista_tfg_ordenados[0][0]]['Turnos']['Aula 1'][dia_preferente][0][1].strftime('%H:%M')]
        del turnos_defensa['Aula 1'][dia_preferente][turnos_defensa['Aula 1'][dia_preferente].index(hora_preferente)]

        i = 0

        for tfg in lista_tfg_ordenados:
            if 'Turno Asignado' not in tfg_dicts[tfg[0]].keys():
                inputs = [[tfg[0], tfg_dicts, profesores, aula, dia, hora[0].strftime('%H:%M'), hora[1].strftime('%H:%M'), dias_formato] for aula in tfg_dicts[tfg[0]]['Turnos'] for dia in  list(tfg_dicts[tfg[0]]['Turnos'][aula]) if tfg_dicts[tfg[0]]['Turnos'][aula][dia] for hora in tfg_dicts[tfg[0]]['Turnos'][aula][dia] if hora in turnos_defensa[aula][dia]]
                pool = mp.Pool(int(mp.cpu_count()/2))
                resultados = pool.starmap(funcion_objetivo, inputs)

                if resultados:
                    minimo = min(resultados)
                    turnos_minimo = [inputs[i][3]+','+inputs[i][4]+','+inputs[i][5]+','+inputs[i][6] for i in [i for i,valor in enumerate(resultados) if valor == minimo]]

                    if len(turnos_minimo) > 1 and i < len(tfg_dicts)-1 and bool_mejores_soluciones:
                        turno = desempate_entre_turnos(tfg_dicts, tfg[0], turnos_minimo, turnos_defensa, profesores, dias_formato, lista_tfg_ordenados)
                        tfg_dicts[tfg[0]]['Turno Asignado'] = turno.split(',')
                    elif bool_mejores_soluciones == False and len(turnos_minimo) > 1 and i < len(tfg_dicts)-1:
                        tfg_dicts[tfg[0]]['Turno Asignado'] = choice(turnos_minimo).split(',')
                    elif len(turnos_minimo) > 1 and i == len(tfg_dicts)-1:
                        tfg_dicts[tfg[0]]['Turno Asignado'] = choice(turnos_minimo).split(',')
                    else:
                        tfg_dicts[tfg[0]]['Turno Asignado'] = turnos_minimo[0].split(',')
                    
                    del turnos_defensa[tfg_dicts[tfg[0]]['Turno Asignado'][0]][tfg_dicts[tfg[0]]['Turno Asignado'][1]][turnos_defensa[tfg_dicts[tfg[0]]['Turno Asignado'][0]][tfg_dicts[tfg[0]]['Turno Asignado'][1]].index([datetime.strptime(tfg_dicts[tfg[0]]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg[0]]['Turno Asignado'][3],'%H:%M')])]
                else:
                    return 2, 'Las horas de inicio y fin son demasiado restrictivas, no se pueden asignar horarios a todos los TFG.'

            #Después del proceso de elección de cada tfg crear una funíón que elimine la hora y el día en el resto de aulas para los tfgs con algun profesor en comun    
            eliminar_turnos_conflictivos(tfg[0], tfg_dicts)

            df.loc[df['Título']==tfg[0], 'Turno Asignado'] = tfg_dicts[tfg[0]]['Turno Asignado'][0] + ' ' + tfg_dicts[tfg[0]]['Turno Asignado'][1] + ' ' + tfg_dicts[tfg[0]]['Turno Asignado'][2] + '-' + tfg_dicts[tfg[0]]['Turno Asignado'][3]

            i = i + 1

        df.to_excel('tfgs.xlsx', index = False)

        return 0, ''
    except IndexError:
        return 1, 'No hay TFG a los que asignar un horario'
    except:
        return 2, 'Error asignando los horarios a los TFG'

def desempate_entre_turnos(tfg_dicts, tfg_dict, turnos_empatados, turnos_defensa, profesores, dias_formato, lista_tfg_ordenados):
    tfgs = deepcopy(tfg_dicts)
    turnos = deepcopy(turnos_defensa)

    valores_turno = {}
    minimos = {}

    for turno in turnos_empatados:
        tfgs[tfg_dict]['Turno Asignado'] = turno.split(',')
        del turnos[tfgs[tfg_dict]['Turno Asignado'][0]][tfgs[tfg_dict]['Turno Asignado'][1]][turnos[tfgs[tfg_dict]['Turno Asignado'][0]][tfgs[tfg_dict]['Turno Asignado'][1]].index([datetime.strptime(tfgs[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfgs[tfg_dict]['Turno Asignado'][3],'%H:%M')])]
        eliminar_turnos_conflictivos(tfg_dict, tfgs)

        inputs = [[lista_tfg_ordenados[lista_tfg_ordenados.index([item for item in lista_tfg_ordenados if tfg_dict in item][0])+1][0], tfgs, profesores, aula, dia, hora[0].strftime('%H:%M'), hora[1].strftime('%H:%M'), dias_formato] for aula in tfg_dicts[tfg_dict]['Turnos'] for dia in  list(tfg_dicts[tfg_dict]['Turnos'][aula]) if tfg_dicts[tfg_dict]['Turnos'][aula][dia] for hora in tfg_dicts[tfg_dict]['Turnos'][aula][dia] if hora in turnos_defensa[aula][dia]]
        pool = mp.Pool(int(mp.cpu_count()/2))
        resultados = pool.starmap(funcion_objetivo, inputs)
        minimos[turno] = min(resultados)

    minimo = min(minimos.values())
    turnos_ret = [turno for turno, valor in minimos.items() if valor == minimo]

    return choice(turnos_ret)

def eliminar_turnos_conflictivos(tfg_dict, tfg_dicts):
    for tfg in tfg_dicts[tfg_dict]['Miembro 1']['TFGs']:
        if tfg in tfg_dicts.keys():
            for aula in tfg_dicts[tfg]['Turnos']:
                if tfg_dicts[tfg_dict]['Turno Asignado'][1] in tfg_dicts[tfg]['Turnos'][aula]:
                    if [datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')] in tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]]:
                        del tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]][tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]].index([datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')])]

    for tfg in tfg_dicts[tfg_dict]['Miembro 2']['TFGs']:
            if tfg in tfg_dicts.keys():
                for aula in tfg_dicts[tfg]['Turnos']:
                    if tfg_dicts[tfg_dict]['Turno Asignado'][1] in tfg_dicts[tfg]['Turnos'][aula]:
                        if [datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')] in tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]]:
                            del tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]][tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]].index([datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')])]

    for tfg in tfg_dicts[tfg_dict]['Miembro 3']['TFGs']:
            if tfg in tfg_dicts.keys():
                for aula in tfg_dicts[tfg]['Turnos']:
                    if tfg_dicts[tfg_dict]['Turno Asignado'][1] in tfg_dicts[tfg]['Turnos'][aula]:
                        if [datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')] in tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]]:
                            del tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]][tfg_dicts[tfg]['Turnos'][aula][tfg_dicts[tfg_dict]['Turno Asignado'][1]].index([datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')])]

def funcion_valor_primer_tfg(tfg_dict, tfg_dicts):
    tfgs_comunes = set([])
    dias_comunes = []
    dias_primer_tfg = []

    #Si hay TFGs con los mismos 3 miembros se priorizan los días que concidan entre ellos
    tfgs_comunes.update(tfg_dicts[tfg_dict]['Miembro 1']['TFGs'])
    tfgs_comunes = tfgs_comunes.intersection(list(set(tfg_dicts[tfg_dict]['Miembro 2']['TFGs']) & set(tfg_dicts[tfg_dict]['Miembro 3']['TFGs'])))
    tfgs_comunes.remove(tfg_dict)

    #Si no, se priorizan los días de los TFGs con 2 miembros en común
    if not list(tfgs_comunes):
        tfgs_comunes.update(tfg_dicts[tfg_dict]['Miembro 1']['TFGs'])
        tfgs_comunes = set(list(tfgs_comunes.intersection(list(set(tfg_dicts[tfg_dict]['Miembro 2']['TFGs'])))) + list(tfgs_comunes.intersection(list(set(tfg_dicts[tfg_dict]['Miembro 3']['TFGs'])))))
        tfgs_comunes.remove(tfg_dict)

    #Si no, de los TFGs de cada miembro se priorizan los días con más turnos
    if not list(tfgs_comunes):
        lista = []
        for dia in sorted(tfg_dicts[tfg_dict]['Turnos']['Aula 1'], key = lambda dia: len(tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia]), reverse = True):
            if tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia]:
                lista.append(dia)
        return lista[0], tfg_dicts[tfg_dict]['Turnos']['Aula 1'][lista[0]][0]

    dias_primer_tfg = []
    for dia in tfg_dicts[tfg_dict]['Turnos']['Aula 1']:
        if tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia]:
            dias_primer_tfg.append(dia) 

    for tfg in list(tfgs_comunes):
        dias_comunes = []
        for dia in tfg_dicts[tfg]['Turnos']['Aula 1']:
            if tfg_dicts[tfg]['Turnos']['Aula 1'][dia] != [] and tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia] != []:
                dias_comunes.append(dia)

    if not dias_comunes:
        dias_comunes = dias_primer_tfg

    dias_mas_turnos = {}
    #Se ordenan todos los días comunes de forma que se da prioridad al día que más turnos disponibles tiene
    tfgs_comunes.update([tfg_dict])
    for dia in dias_comunes:
        for tfg in list(tfgs_comunes):
            for dia in tfg_dicts[tfg]['Turnos']['Aula 1']:
                if tfg_dicts[tfg]['Turnos']['Aula 1'][dia]:
                    if dia in dias_mas_turnos.keys():
                        dias_mas_turnos[dia] += len(tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia])
                    else:
                        dias_mas_turnos[dia] = len(tfg_dicts[tfg_dict]['Turnos']['Aula 1'][dia])

    maximo = max(dias_mas_turnos.values())
    dias = [dia for dia, turnos in dias_mas_turnos.items() if turnos == maximo]

    dia_definitivo = choice(dias)
    aula = choice(list(tfg_dicts[tfg_dict]['Turnos'].keys()))

    return dia_definitivo, tfg_dicts[tfg_dict]['Turnos'][aula][dia_definitivo][0]

#Funcionar a minimizar, se penaliza que los tfgs de los mismos profesores estén separados en días y turnos dentro del mismo día, además de en distintas aulas en el mismo día
def funcion_objetivo(tfg_dict, tfgs, profesores, aula, dia, hora_inicio, hora_fin, dias_formato):
    peso_dias_distintos = 5
    peso_horas_distintas = 1

    #Este sólo aplicarlo en caso de que sean el mismo día en horas consecutivas
    peso_aulas_distintas = 15

    #Peso defensas simultáneas para aprovechar todas las aulas disponibles
    peso_defensas_simultaneas = -2

    #Valor de la función para todos los profesores
    valor = 0

    #Copia de los tfgs
    tfg_dicts = deepcopy(tfgs)

    #Asignar al tfg correspondiente el supuesto día y hora
    tfg_dicts[tfg_dict]['Turno Asignado'] = [aula, dia, hora_inicio, hora_fin]

    for nombre in profesores:
        tfg_comunes = list(set(profesores[nombre]['TFGs'].copy()) - (set(profesores[nombre]['TFGs'].copy()) - set(tfg_dicts.keys())))

        tfg_no_comunes = list(set(tfg_dicts.keys()) - set(tfg_comunes))

        for i_tfg in range(len(tfg_comunes)):
            for j_tfg in range(i_tfg + 1, len(tfg_comunes)):
                if 'Turno Asignado' in tfg_dicts[tfg_comunes[i_tfg]].keys() and 'Turno Asignado' in tfg_dicts[tfg_comunes[j_tfg]].keys():
                    #Penalización por separación de dias
                    #aux1 = abs((dias_formato[tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][1]] - dias_formato[tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][1]]).days) * peso_dias_distintos
                    aux1 = peso_dias_distintos
                    #Penalización por separación de horas
                    if datetime.strptime(tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][2], '%H:%M') > datetime.strptime(tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][2], '%H:%M'):
                        aux2 = datetime.strptime(tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][2], '%H:%M') - datetime.strptime(tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][2], '%H:%M')
                    elif datetime.strptime(tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][2], '%H:%M') < datetime.strptime(tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][2], '%H:%M'):
                        aux2 = datetime.strptime(tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][2], '%H:%M') - datetime.strptime(tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][2], '%H:%M')
                    else:
                        aux2 = 0

                    #Penalización por separación de aulas dentro de un mismo día
                    if aux1 == 0 and tfg_dicts[tfg_comunes[i_tfg]]['Turno Asignado'][0] != tfg_dicts[tfg_comunes[j_tfg]]['Turno Asignado'][0]:
                        aux3 = peso_aulas_distintas
                    else:
                        aux3 = 0

                    valor = valor + aux1 + aux3 + int(str(aux2).split(':')[0]) * peso_horas_distintas

        for k_tfg in range(len(tfg_comunes)):
            for l_tfg in range(len(tfg_no_comunes)):
                if 'Turno Asignado' in tfg_dicts[tfg_comunes[k_tfg]].keys() and 'Turno Asignado' in tfg_dicts[tfg_no_comunes[l_tfg]].keys():
                    if abs(int(tfg_dicts[tfg_comunes[k_tfg]]['Turno Asignado'][1].split(' ')[1]) - int(tfg_dicts[tfg_no_comunes[l_tfg]]['Turno Asignado'][1].split(' ')[1])) == 0 and tfg_dicts[tfg_comunes[k_tfg]]['Turno Asignado'][0] != tfg_dicts[tfg_no_comunes[l_tfg]]['Turno Asignado'][0]:
                        valor = valor + peso_defensas_simultaneas

    return valor

def horarios(correo, contrasena, bool_tutorias, bool_docencia, bool_planificacion, bool_mejores_soluciones, n_aulas, n_cuatrimestre, path_listado, path_restricciones):
    cod_error = 0

    #Se lee el archivo de configuración
    config_dict = {}
    leer_configuracion(config_dict)

    #Se debería de hacer una limpieza del directorio resources en cada ejecución y después eliminarlo
    if FileSystem().does_directory_not_exist(config_dict['directorio_resources']) == True:
        FileSystem().create_directory(config_dict['directorio_resources'])

    dia_inicio_defensas, dia_fin_defensas, lista_dias, cod_error, msje_error = fechas(config_dict['url_calendario'], config_dict['directorio_resources'], config_dict['headless'] == 'True', int(n_cuatrimestre), config_dict['fecha_inicio'])

    if cod_error!= 0:
        return cod_error, msje_error

    #Llamar a función que crear listas de días y horas de defensas
    turnos_defensa = {}
    calendario(config_dict['hora_inicio'], config_dict['hora_fin'], config_dict['hora_inicio_descanso'], config_dict['hora_fin_descanso'], config_dict['duracion'], int(n_aulas), lista_dias.values(), turnos_defensa)

    tfg_dicts, lista_tfg_no_web, cod_error, msje_error = tfg(config_dict['url_tfg'], path_listado, correo, contrasena, config_dict['headless'] == 'True')
    if cod_error!= 0:
        return cod_error, msje_error

    for tfg_dict in tfg_dicts:
        #Si existe un archivo con TFG a los que ya se les ha asignado horario, eliminar estos de la lista
        if FileSystem().does_file_exist("tfgs_asignados.xlsx") == True:
            l, cod_error, msje_error = listado("tfgs_asignados.xlsx")
            
            keys = []
            for key in l[1].keys():
                keys.append(key) 
            if cod_error!= 0:
                return cod_error, msje_error

            for dic in l:
                if tfg_dict == dic['Título'] and dic['Turno Asignado'] != 'Horario no disponible':
                    tfg_dicts[tfg_dict]['Turno Asignado'] = [' '.join(dic['Turno Asignado'].split(' ')[0:2]), ' '.join(dic['Turno Asignado'].split(' ')[2:4]), dic['Turno Asignado'].split(' ')[4].split('-')[0], dic['Turno Asignado'].split(' ')[4].split('-')[1]]
                    del turnos_defensa[tfg_dicts[tfg_dict]['Turno Asignado'][0]][tfg_dicts[tfg_dict]['Turno Asignado'][1]][turnos_defensa[tfg_dicts[tfg_dict]['Turno Asignado'][0]][tfg_dicts[tfg_dict]['Turno Asignado'][1]].index([datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][2],'%H:%M'), datetime.strptime(tfg_dicts[tfg_dict]['Turno Asignado'][3],'%H:%M')])]
            del l

    #tfg_dicts['Estudio de viabilidad de una instalación fotovoltaica de autoconsumo en un parking de superficie con puntos de recarga para vehículos eléctricos']['Miembro 3'] = 'Rebeca_Herrero_Martin'
    #tfg_dicts['Desarrollo del firmware para el control de luces de un cargador de vehículos eléctricos']['Miembro 3'] = 'Pedro_Luis_Castedo_Cepeda'
    #tfg_dicts['Modelado de módulo translúcido de concentración fotovoltaica y análisis de viabilidad de uso como luminaria en edificios.']['Miembro 2'] = 'Ruben_Nuñez_Judez'

    #Leer horarios de los miembros del tribunal en la web de tutorías
    nombres = []
    set_nombres = set(nombres)
    for tfg_dict in tfg_dicts:
        set_nombres.update([tfg_dicts[tfg_dict]['Miembro 1'], tfg_dicts[tfg_dict]['Miembro 2'], tfg_dicts[tfg_dict]['Miembro 3']])
        
    nombres = list(set_nombres)

    if (bool_docencia == 'True' or bool_tutorias == 'True'):
        #Hay que duplicar el nombre del primer profesor de la lista porque el robot de rps no consigue obtener el href del primer profesor correctamente
        nombres.append(nombres[0])
        lista_profesores_sin_horario, cod_error, msje_error = tutorias(nombres, config_dict['url_tutorias'], config_dict['directorio_resources'], config_dict['headless'] == 'True')
        if cod_error!= 0:
            return cod_error, msje_error
        nombres.remove(nombres[0])

        for profesor in lista_profesores_sin_horario:
            for tfg_dict in tfg_dicts:
                if profesor in tfg_dicts[tfg_dict].values():
                    del tfg_dicts[tfg_dict]

    profesores = {}

    for nombre in nombres:
        nombre_dict = {}
        nombre_dict['Nombre'] = nombre
        nombre_dict['Dataframes'], cod_error, msje_error = dataframe_horarios_profesor(nombre, bool_tutorias == 'True', bool_docencia == 'True', bool_planificacion == 'True', path_restricciones, config_dict['directorio_resources'])
        if cod_error!= 0:
            return cod_error, msje_error
        nombre_dict['TFGs'] = []

        for tfg_dict in tfg_dicts:
            if nombre in tfg_dicts[tfg_dict].values():
                nombre_dict['TFGs'].append(tfg_dict)

        profesores[nombre] = nombre_dict
                    
    #Recoge los horarios de los tres profesores del tribunal
    for tfg_dict in tfg_dicts:
        tfg_dicts[tfg_dict]['Miembro 1'] = profesores[tfg_dicts[tfg_dict]['Miembro 1']]
        tfg_dicts[tfg_dict]['Miembro 2'] = profesores[tfg_dicts[tfg_dict]['Miembro 2']]
        tfg_dicts[tfg_dict]['Miembro 3'] = profesores[tfg_dicts[tfg_dict]['Miembro 3']]

        
        tfg_dicts[tfg_dict]['Turnos'] = eliminar_horarios(tfg_dicts[tfg_dict], turnos_defensa, bool_tutorias == 'True', bool_docencia == 'True', bool_planificacion == 'True')
        if cod_error!= 0:
            return cod_error, msje_error

    for tfg_dict in tfg_dicts:
        if 'Turno Asignado' in tfg_dicts[tfg_dict].keys():
            eliminar_turnos_conflictivos(tfg_dict, tfg_dicts)

    eliminar_tfg_sin_disponibilidad(tfg_dicts)

    if tfg_dicts.keys():
        cod_error, msje_error = asignar_horarios(tfg_dicts, turnos_defensa, profesores, dia_inicio_defensas, dia_fin_defensas, bool_mejores_soluciones == 'True')
        if cod_error!= 0:
            return cod_error, msje_error

    if FileSystem().does_directory_exist(config_dict['directorio_resources']) == True:
        FileSystem().remove_directory(config_dict['directorio_resources'], recursive=True)

    if cod_error == 0:
        msje_error = 'Asignación de horarios completada con éxito'
        
    return cod_error, msje_error